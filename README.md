# 📰 FCC APIs and Microservices Projects and Certification

<img src="https://gitlab.com/yemrekeskin/Fcc-APIs-and-Microservices-Cert/raw/8de979708779595ac44ca5bdcf5bd80e7dd51266/cert.PNG"> 

## Circumstances

- APIs and Microservices Certification
  - Managing Packages with Npm
  - Basic Node and Express
  - MongoDb and Mongoose
  - APIs and Microservice Projects

## Certification
  
- Freecodecamp Portfolio - https://www.freecodecamp.org/yemrekeskin
- Certification Link - https://www.freecodecamp.org/certification/yemrekeskin/apis-and-microservices

## Projects

- 🕙 FCC APIs and Microservices Projects - Build a Timestamp Microservice 
  - https://yemrekeskin-fcc-timestamp.glitch.me/
  - https://gitlab.com/yemrekeskin/fcc-timestamp
- ✂️ FCC APIs and Microservices Projects - Build URL Shortener Microservice  
  - https://yemrekeskin-fcc-urlshortener.glitch.me/
  - https://gitlab.com/yemrekeskin/fcc-urlshortener
- 🔍 FCC APIs and Microservices Projects - Build Request Header Parser Microservice
  - https://yemrekeskin-fcc-headerparser.glitch.me/api/whoami
  - https://gitlab.com/yemrekeskin/fcc-headerparser
- 🏃 FCC APIs and Microservices Projects - Build a Exercise Tracker
  - https://yemrekeskin-fcc-exercisetracker.glitch.me/
  - https://gitlab.com/yemrekeskin/fcc-exercisetracker
- 📁 FCC APIs and Microservices Projects - Build File Metadata Microservice
  - https://yemrekeskin-fcc-filemetadata.glitch.me/
  - https://gitlab.com/yemrekeskin/fcc-filemetadata
  
